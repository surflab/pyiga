import numpy as np
from scipy.sparse import coo_matrix,csr_matrix,find
import scipy.linalg as linalg
from mayavi.mlab import *

class Mesh:
    def __init__(self, vertices,faces):
        self.v = vertices
        self.f = faces
        self.nv = vertices.shape[0]
        self.nf = faces.shape[0]
        self.ngon = faces.shape[1]
        row = np.zeros(self.nf*self.ngon,dtype = int)
        col = np.zeros(self.nf*self.ngon,dtype = int)
        data = np.zeros(self.nf*self.ngon,dtype = int)
        for i,f in enumerate(faces):
            for j in range(self.ngon):
                row[i*self.ngon + j] = f[j]
                col[i*self.ngon + j] = f[(j+1)%self.ngon]
                data[i*self.ngon + j] = i+1 # caution ! face index is from 1 not 0
        self.e = coo_matrix((data,(row,col)), shape=(self.nv,self.nv)).tocsr()
        self.isboundaryv = np.zeros(self.nv,dtype=bool)
        self.isboundaryf = np.zeros(self.nf,dtype=bool)
        for i,f in enumerate(faces):
            for j in range(self.ngon):
                v1 = f[j]
                v2 = f[(j+1)%self.ngon]
                if (self.e[v2,v1] == 0):  
                    self.e[v2,v1] = -1 # outter face has index -1
                    self.isboundaryf[i] = 1
                    self.isboundaryv[v1] = 1;
                    self.isboundaryv[v2] = 1;
        self.valence = np.zeros(self.nv,dtype = int)
        for i,f in enumerate(vertices):
            [_,j,_] = find(self.e[i,:])
            self.valence[i] = len(j) 
        self.eov = [s for s,x in enumerate(self.valence) if ((not x==4) and self.isboundaryv[s])] 
    def oneringv(self,v):
        [_,j,_] = find(self.e[v,:])
        return j
    def oneringf(self,v):
        a = self.e[v,:][self.e[v,:] > 0]
        b = self.e[:,v][self.e[:,v] > 0]
        return np.union1d(np.array(a).reshape(-1), np.array(b).reshape(-1))
    def draw(self, rep = 'wireframe', showIndices = True):
        tf = self.f   
        if (self.ngon == 4):
            tf = np.r_[self.f[:,0:3],self.f[:,[0,2,3]]]
        fig = triangular_mesh(self.v[:,0],self.v[:,1],self.v[:,2],tf,representation = rep)
        if (showIndices):
            fig.scene.disable_render = True
            for s in range(self.nv):
                text3d(self.v[s,0],self.v[s,1],self.v[s,2], str(s), scale = 0.01) 
            fig.scene.disable_render = False
        return fig
    def refine(self):
        if (self.ngon == 4):
            [i,j,_] = find(self.e)
            [_,idx,_] = find(i<j)
            ne = len(idx)
            row = np.zeros(ne,dtype = int)
            col = np.zeros(ne,dtype = int)
            data = np.zeros(ne,dtype = int)
            for k in range(ne):
                row[k] = i[idx[k]]
                col[k] = j[idx[k]]
                data[k] = k
            edge_idx = coo_matrix((data,(row,col)), shape=(self.nv,self.nv)).tocsr()
            dim = len(self.v[0,:])
            v = np.zeros([self.nv+self.nf+ne,dim],dtype=float)
            f = np.zeros([4*self.nf,4],dtype=int)
            for pp in range(self.nf):
                v1 = self.f[pp,0]
                v2 = self.f[pp,1]
                v3 = self.f[pp,2]
                v4 = self.f[pp,3]
                v5 = (edge_idx[v1,v2] if (v1 < v2) else edge_idx[v2,v1]) + self.nv
                v6 = (edge_idx[v2,v3] if (v2 < v3) else edge_idx[v3,v2]) + self.nv
                v7 = (edge_idx[v3,v4] if (v3 < v4) else edge_idx[v4,v3]) + self.nv
                v8 = (edge_idx[v4,v1] if (v4 < v1) else edge_idx[v1,v4]) + self.nv
                v[v1,:] = self.v[v1,:]
                v[v2,:] = self.v[v2,:]
                v[v3,:] = self.v[v3,:]
                v[v4,:] = self.v[v4,:]
                v[v5,:] = (self.v[v1,:] + self.v[v2,:])/2
                v[v6,:] = (self.v[v2,:] + self.v[v3,:])/2
                v[v7,:] = (self.v[v3,:] + self.v[v4,:])/2
                v[v8,:] = (self.v[v4,:] + self.v[v1,:])/2
                v9 = self.nv+ne+pp 
                v[v9,:] = (self.v[v1,:] + self.v[v2,:] + self.v[v3,:] + self.v[v4,:])/4
                f[4*pp] = np.array([v1,v5,v9,v8])
                f[4*pp+1] = np.array([v5,v2,v6,v9])
                f[4*pp+2] = np.array([v8,v9,v7,v4])
                f[4*pp+3] = np.array([v9,v6,v3,v7])
            return Mesh(v,f)

def read_off(filename):
    file = open(filename,'r')
    if 'OFF' != file.readline().strip():
        print 'Not a valid OFF header'
    nv, nf, n_dontknow = tuple([int(s) for s in file.readline().strip().split(' ')])
    
    tmp = [float(s) for s in file.readline().strip().split(' ')]
   
    ncoord = len(tmp)
    v = np.empty([nv,ncoord],dtype = float)
    v[0] = tmp
    for i in range(nv-1):
        v[i+1] =  [float(s) for s in file.readline().strip().split(' ')]

    tmp = [int(s) for s in file.readline().strip().split(' ')][1:]
    ngon = len(tmp)
    f = np.empty([nf,ngon],dtype = int)
    f[0] = tmp
    for i in range(nf-1):
        f[i+1] = [int(s) for s in file.readline().strip().split(' ')][1:]
    file.close()
    return v, f

from operator import mul    # or mul=lambda x,y:x*y
from fractions import Fraction

def nCk(n,k): 
  return int( reduce(mul, (Fraction(n-i, i+1) for i in range(k)), 1) )

def patchView(G,res,rep,c = (1,0,0)): # for debugging, no real use
    u = np.linspace(0,1,res)    
    order = len(G[0][:,0])
    b = np.zeros([order, res])
    for i in range(order):
        b[i,:] = nCk(order-1,i)*(np.power(1-u,order-1-i)*np.power(u,i))
    cx = G[0]
    cy = G[1]
    cw = np.ones_like(G[0])
    X = np.dot(np.dot(np.transpose(b),(cx*cw) ),b)
    Y = np.dot(np.dot(np.transpose(b),(cy*cw) ),b)
    W = np.dot(np.dot(np.transpose(b),cw),b)
    X = X/W
    Y = Y/W
    Z = np.zeros_like(X)
    return mesh(X,Y,Z, representation = rep, color = c)
          
def bezierView(G, res, rep = 'surface', showCurve = False, showNet = False): # G is a list of matrices of coefficients, res is resolution
    u = np.linspace(0,1,res);    
    bb = [];
    for d in range(3,6): # consider patches with degree 3,4 or 5
        b = np.zeros([d+1, res])
        for i in range(d+1):
            b[i,:] = nCk(d,i)*(np.power(1-u,d-i)*np.power(u,i))
        bb.append(b)
    nP = len(G);
    dim = len(G[0])
    fig = figure('Surface')
    fig.scene.disable_render = True
    for pp in range(nP):
        cx = G[pp][0]
        cy = G[pp][1]
        cz = np.zeros_like(G[pp][0])
        if (dim > 3): cz = G[pp][2]
        cw = G[pp][dim-1]
        od = len(cx[:,0]);
        b = bb[od-4];
        X = np.dot(np.dot(np.transpose(b),(cx*cw) ),b) # b'*(cx.*cw)*b
        Y = np.dot(np.dot(np.transpose(b),(cy*cw) ),b)
        Z = np.dot(np.dot(np.transpose(b),(cz*cw) ),b)
        W = np.dot(np.dot(np.transpose(b),cw),b)
        X = X/W
        Y = Y/W
        Z = Z/W
        if (not showCurve): 
            mesh(X,Y,Z,representation = rep)
        else:
            plot3d(X[:,0],Y[:,0],Z[:,0])
            plot3d(X[:,od-1],Y[:,od-1],Z[:,od-1])
            plot3d(X[0,:],Y[0,:],Z[0,:])
            plot3d(X[od-1,:],Y[od-1,:],Z[od-1,:])
        if (showNet):
            for d in range(od):
                plot3d(cx[:,i],cy[:,i],cz[:,i])
                plot3d(cx[i,:],cy[i,:],cz[i,:])

    fig.scene.disable_render = False
    return fig

def bezierExtraction(iG,mesh,algorithm = 'simple'): # generate structure for both analysis and surface
    nP = mesh.nf
    nV = mesh.nv
    eop = mesh.eov # extraordinary point (vertex)
    nEOP = len(eop)
    nPat = nP # number of patches, may not equal to nP if we use 2x2 split
    G = [None]*nPat; # Geometry structure
    DoFM = [None]*nPat # DoF map, stores contribution of B-spline-like points to a Bezier point
    for i in range(nPat):
        G[i] = [None]*4
        DoFM[i] = [None]*4
        for j in range(4):
            G[i][j] = np.zeros([4,4])
            DoFM[i][j] = [None]*4
            for k in range(4):
                DoFM[i][j][k] = [None]*2
    bdy_v = [s for s,x in enumerate(mesh.isboundaryv) if (x==True)]
    nBdy = len(bdy_v)
    bdy_map = np.zeros(nV,dtype=int)
    k = 0
    for i in range(nBdy):
        bdy_map[bdy_v[i]] = k
        k = k + 3 if (mesh.valence[bdy_v[i]] < 3) else k + 1
    nDoF = nV + k # number of degrees of freedom
    bdy_ind = range(nV,nDoF)
    
    dim = len(mesh.v[0,:])
    idxmap = [[0, 0, 0, 1, 1, 1, 1, 0],[0, 3, 1, 3, 1, 2, 0, 2],[3, 3, 3, 2, 2, 2, 2, 3],[3, 0, 2, 0, 2, 1, 3, 1]]
    for  v in range(nV):
        if (not mesh.isboundaryv[v]):
            n = mesh.valence[v];
            vidx = np.zeros(2*n+1,dtype=int)
            fidx = np.zeros(n,dtype=int)
            ov = mesh.oneringv(v)
            bv = ov[mesh.isboundaryv[ov]]
            type = len(bv)
            v1 = ov[0];
            if (type == 0):
                w = np.asarray([4,1]*n + [n*n], dtype=float)/(n*(n+5))
                w1 = np.asarray([[2,1,4,1,2,8]]*n,dtype=float)/18
                w2 = np.asarray([[2,1,2,4]]*n,dtype=float)/9
            elif (type == 1):
                v1 = bv[0];
                w = np.array([12,3,7,2,8,2,7,3,28],dtype=float)/72;
                w1 = np.array([np.array([1, 1, 4, 1, 1, 4],dtype=float)/12, 
                    np.array([6, 3, 7, 2, 4, 14],dtype=float)/36, 
                    np.array([2, 1, 4, 1, 2, 8],dtype=float)/18, 
                    np.array([4, 2, 7, 3, 6, 14],dtype=float)/36])
                w2 = np.array([np.array([2, 1, 1, 2],dtype=float)/6,
                    np.array([2, 1, 2, 4],dtype=float)/9,
                    np.array([2, 1, 2, 4],dtype=float)/9,
                    np.array([1, 1, 2, 2],dtype=float)/6])
            else:
               v1 = bv[0] if (mesh.e[v,bv[0]] == mesh.e[bv[1],v]) else bv[1]
               w = np.array([21,9,21,6,14,4,14,6,49],dtype=float)/144;
               w1 = np.array([np.array([2, 2, 7, 3, 3, 7,],dtype=float)/24, 
                    np.array([3, 3, 7, 2, 2, 7],dtype=float)/24, 
                    np.array([6, 3, 7, 2, 4, 14],dtype=float)/36, 
                    np.array([4, 2, 7, 3, 6, 14],dtype=float)/36])
               w2 = np.array([np.array([1, 1, 1, 1],dtype=float)/4,
                    np.array([2, 1, 1, 2],dtype=float)/6,
                    np.array([2, 1, 2, 4],dtype=float)/9,
                    np.array([1, 1, 2, 2],dtype=float)/6])

            for k in range(n):
               f = mesh.e[v,v1]-1
               lid = [s for s,x in enumerate(mesh.f[f,:]) if (x==v)][0] # search for the local index in the face
               vidx[2*k] = v1
               vidx[2*k+1] = mesh.f[f,(lid+2)%4]
               fidx[k] = f
               v1 = mesh.f[f,(lid+3)%4]
            vidx[2*n] = v
            p = np.dot(np.transpose(w),mesh.v[vidx,:])
            p[0:dim-1] = p[0:dim-1]/p[dim-1]
            for k in range(n):
                f = fidx[k]
                pk = (k-1)%n
                f1 = fidx[pk]
                lid = [s for s,x in enumerate(mesh.f[f,:]) if (x==v)][0]
                idx = idxmap[lid]
                for ii in range(dim):
                    G[f][ii][idx[0],idx[1]] = p[ii]
                DoFM[f][idx[0]][idx[1]][0] = vidx # degrees of freedom
                DoFM[f][idx[0]][idx[1]][1] = w # weights of contribution
                
                v1 = mesh.f[f,(lid+3)%4]
                vl = [vidx[2*pk],vidx[2*pk+1],vidx[2*k],vidx[2*k+1],v1,v]
                pp = np.dot(w1[k,:],mesh.v[vl,:]) 
                pp[0:dim-1] = pp[0:dim-1]/pp[dim-1]
                for ii in range(4):
                    G[f][ii][idx[2],idx[3]] = pp[ii]
                DoFM[f][idx[2]][idx[3]][0] = vl; 
                DoFM[f][idx[2]][idx[3]][1] = w1[k,:]

                lid1 = [s for s,x in enumerate(mesh.f[f1,:]) if (x==v)][0]
                idx1 = idxmap[lid1]
                for ii in range(dim):
                    G[f1][ii][idx1[6],idx1[7]] = pp[ii]
                DoFM[f1][idx1[6]][idx1[7]][0] = vl; 
                DoFM[f1][idx1[6]][idx1[7]][1] = w1[k,:];
            
                vl = [vidx[2*k],vidx[2*k+1],v1,v];
                pp = np.dot(w2[k,:],mesh.v[vl,:]) 
                pp[0:dim-1] = pp[0:dim-1]/pp[dim-1];
                for ii in range(dim): 
                     G[f][ii][idx[4],idx[5]] = pp[ii]
                DoFM[f][idx[4]][idx[5]][0] = vl; 
                DoFM[f][idx[4]][idx[5]][1] = w2[k,:]
        else:
            if (mesh.valence[v] > 2):
                ov = mesh.oneringv(v)
                v1 = ov[~mesh.isboundaryv[ov]][0]
                f1 = mesh.e[v,v1]-1;
                lid1 = [s for s,x in enumerate(mesh.f[f1,:]) if (x==v)][0]
                v2 = mesh.f[f1,(lid1+3)%4]
                f2 = mesh.e[v1,v]-1
                lid2 = [s for s,x in enumerate(mesh.f[f2,:]) if (x==v)][0]
                v3 = mesh.f[f2,(lid2+1)%4]
            
                idx1 = idxmap[lid1]
                idx2 = idxmap[lid2]
                for ii in range(dim): 
                    G[f1][ii][idx1[0],idx1[1]] = iG[f1][ii][idx1[0],idx1[1]]
                    G[f1][ii][idx1[6],idx1[7]] = iG[f1][ii][idx1[6],idx1[7]]
                    G[f2][ii][idx2[0],idx2[1]] = iG[f2][ii][idx2[0],idx2[1]]
                    G[f2][ii][idx2[2],idx2[3]] = iG[f2][ii][idx2[2],idx2[3]]
            
                vl = [v2, v, v3]
                DoFM[f1][idx1[0]][idx1[1]][0] = bdy_map[vl]+np.asarray([mesh.valence[v2]==2]+[0]+[2*(mesh.valence[v3]==2)],dtype=int)+nV
                DoFM[f2][idx2[0]][idx2[1]][0] = bdy_map[vl]+np.asarray([mesh.valence[v2]==2]+[0]+[2*(mesh.valence[v3]==2)],dtype=int)+nV
            
                w = np.asarray([1, 4, 1])/6.0
                w1 = np.asarray([1, 2])/3.0
                if (mesh.valence[v2] == 2):
                    w = np.asarray([3, 7, 2])/12.0
                    w1 = np.asarray([1,  1])/2.0
            
                vl1  = [v2, v]
                p = np.dot(w1,mesh.v[vl1,:]) 
                p[0:dim-1] = p[0:dim-1]/p[dim-1]
                DoFM[f1][idx1[4]][idx1[5]][0] = vl1
                DoFM[f1][idx1[4]][idx1[5]][1] = w1
                DoFM[f1][idx1[6]][idx1[7]][0] = bdy_map[vl1] + np.asarray([mesh.valence[v2]==2,0], dtype=int)+nV;
                DoFM[f1][idx1[6]][idx1[7]][1] = w1; 
                for ii in range(dim): 
                    G[f1][ii][idx1[4],idx1[5]] = p[ii]
                        
                w1 = np.asarray([1,2])/3.0;
                if (mesh.valence[v3] == 2):
                    w = np.asarray([2, 7, 3])/12.0
                    w1 = np.asarray([1,  1])/2.0
            
                vl2 = [v3, v];
                p = np.dot(w1,mesh.v[vl2,:]) 
                p[0:dim-1] = p[0:dim-1]/p[dim-1]
                DoFM[f2][idx2[4]][idx2[5]][0] = vl2
                DoFM[f2][idx2[4]][idx2[5]][1] = w1
                DoFM[f2][idx2[2]][idx2[3]][0] = bdy_map[vl2] + np.asarray([2*(mesh.valence[v3]==2),0],dtype=int)+nV
                DoFM[f2][idx2[2]][idx2[3]][1] = w1 
                for ii in range(dim):
                    G[f2][ii][idx2[4],idx2[5]] = p[ii]
            
                DoFM[f1][idx1[0]][idx1[1]][1] = w
                DoFM[f2][idx2[0]][idx2[1]][1] = w
            
                p = np.dot(w,mesh.v[vl,:]) 
                p[0:dim-1] = p[0:dim-1]/p[dim-1]
                DoFM[f1][idx1[2]][idx1[3]][0] = vl
                DoFM[f1][idx1[2]][idx1[3]][1] = w
                DoFM[f2][idx2[6]][idx2[7]][0] = vl
                DoFM[f2][idx2[6]][idx2[7]][1] = w
                for ii in range(dim): 
                    G[f1][ii][idx1[2],idx1[3]] = p[ii]
                    G[f2][ii][idx2[6],idx2[7]] = p[ii]
            else:
                f = mesh.oneringf(v)[0] - 1
                lid = [s for s,x in enumerate(mesh.f[f,:]) if (x==v)][0]
                idx = idxmap[lid]
                for ii in range(dim): 
                    G[f][ii][idx[0],idx[1]] = iG[f][ii][idx[0],idx[1]]
                    G[f][ii][idx[2],idx[3]] = iG[f][ii][idx[2],idx[3]]
                    G[f][ii][idx[6],idx[7]] = iG[f][ii][idx[6],idx[7]]
                
                DoFM[f][idx[0]][idx[1]][0] = [bdy_map[v] + nV]
                DoFM[f][idx[0]][idx[1]][1] = 1
                DoFM[f][idx[2]][idx[3]][0] = [bdy_map[v] + 1 + nV]
                DoFM[f][idx[2]][idx[3]][1] = 1
                DoFM[f][idx[6]][idx[7]][0] = [bdy_map[v] + 2 + nV]
                DoFM[f][idx[6]][idx[7]][1] = 1
            
                p = mesh.v[v,:] 
                p[0:dim-1] = p[0:dim-1]/p[dim-1]
                DoFM[f][idx[4]][idx[5]][0] = [v]
                DoFM[f][idx[4]][idx[5]][1] = 1
                for ii in range(dim): 
                    G[f][ii][idx[4],idx[5]] = p[ii]
    return G, DoFM, nDoF, nPat, bdy_ind, bdy_map

def basisView(G, nDoF, DoFM, bid, res, showStencil = False):
    u = np.linspace(0,1,res);    
    d = 3
    b = np.zeros([d+1, res])
    for i in range(d+1):
        b[i,:] = nCk(d,i)*(np.power(1-u,d-i)*np.power(u,i))
    nP = len(G)
    fig = figure('Basis %d '%bid)
    fig.scene.disable_render = True
    z = np.zeros(nDoF)              
    z[bid] = 1
    dim = len(G[0])
    for pp in range(nP):
        cx = G[pp][0]
        cy = G[pp][1]
        cw = G[pp][dim-1]
        cx = cx*cw
        cy = cy*cw
        X = np.dot(np.dot(np.transpose(b),(cx*cw) ),b) # b'*(cx.*cw)*b        
        Y = np.dot(np.dot(np.transpose(b),(cy*cw) ),b) 
        W = np.dot(np.dot(np.transpose(b),cw ),b) 
        X = X/W
        Y = Y/W
        cz = np.zeros_like(cx)
        for i1 in range(d+1):
            for i2 in range(d+1):
                dof = DoFM[pp][i1][i2][0]
                w = DoFM[pp][i1][i2][1]
                cz[i1,i2] = np.dot(w,z[dof])
        if (not showStencil):
            Z = np.dot(np.dot(np.transpose(b),cz ),b)
            mesh(X,Y,Z,vmin = 0, vmax = 1)
        else:
            mesh(cx,cy,np.zeros_like(cx), representation = 'wireframe')
            for s1 in range(d+1):
                for s2 in range(d+1):
                        text3d(cx[s1,s2],cy[s1,s2],0,str(Fraction(cz[s1,s2]).limit_denominator()), scale = 0.05)
    fig.scene.disable_render = False
    return fig
def DeCasteljau(B,var,val): 
    # var should be 'u' or 'v' 
    # val should be in [0,1]
    dim = len(B)
    n = len(B[0][:,0]) # order
    B_out = [None]*2
    for i in range(2):
        B_out[i] = [None]*dim
        for j in range(dim):
            B_out[i][j] = np.zeros([n,n],dtype=float)
    K = [None]*dim
    for c in range(dim):
        K[c] = [None]*n
    for i in range(n):
        for c in range(dim):
            if (var == 'u'):
                K[c][0] = B[c][:,i]
                B_out[0][c][0,i] = B[c][0,i]
                B_out[1][c][n-1,i] = B[c][n-1,i]
            else:
                K[c][0] = B[c][i,:]
                B_out[0][c][i,0] = B[c][i,0]
                B_out[1][c][i,n-1] = B[c][i,n-1]
        for j in range(1,n):
            K[dim-1][j] = (1-val)*K[dim-1][j-1][0:n-j] + val*K[dim-1][j-1][1:n+1-j] # weight
            for kk in range(dim-1):
                K[kk][j] = (1-val)*(K[kk][j-1][0:n-j]*K[dim-1][j-1][0:n-j]) + val*(K[kk][j-1][1:n+1-j]*K[dim-1][j-1][1:n+1-j])
                K[kk][j] = K[kk][j]/K[dim-1][j]
            for c in range(dim):
                if (var == 'u'):
                    B_out[0][c][j,i] = K[c][j][0]
                    B_out[1][c][n-1-j,i] = K[c][j][n-1-j]
                else:
                    B_out[0][c][i,j] = K[c][j][0]
                    B_out[1][c][i,n-1-j] = K[c][j][n-1-j]                                  
    return B_out             
def subdivideBezier(B):
    temp = DeCasteljau(B, 'u', 0.5)
    B1 = DeCasteljau(temp[0], 'v', 0.5)
    B2 = DeCasteljau(temp[1], 'v', 0.5)
    return B1[0],B1[1],B2[0],B2[1]

def genBilinear(mesh):
    nP = mesh.nf
    G = [None]*nP
    dim = len(mesh.v[0,:])
    for pp in range(nP):
        ff = mesh.f[pp,:]
        G[pp] = [None]*dim
        for dd in range(dim):
            G[pp][dd] = np.zeros([4,4],dtype=float)
            p = mesh.v[ff[0],:]
            G[pp][dd][0,0] = p[dd]
            p = mesh.v[ff[1],:]
            G[pp][dd][0,3] = p[dd]
            p = mesh.v[ff[2],:]
            G[pp][dd][3,3] = p[dd]
            p = mesh.v[ff[3],:]
            G[pp][dd][3,0] = p[dd]
            p = (2*mesh.v[ff[0],:] + mesh.v[ff[1],:])/3
            G[pp][dd][0,1] = p[dd]
            p = (mesh.v[ff[0],:] + 2*mesh.v[ff[1],:])/3
            G[pp][dd][0,2] = p[dd]
            p = (2*mesh.v[ff[0],:] + mesh.v[ff[3],:])/3
            G[pp][dd][1,0] = p[dd]
            p = (mesh.v[ff[0],:] + 2*mesh.v[ff[3],:])/3
            G[pp][dd][2,0] = p[dd]
            p = (2*mesh.v[ff[1],:] + mesh.v[ff[2],:])/3
            G[pp][dd][1,3] = p[dd]
            p = (mesh.v[ff[1],:] + 2*mesh.v[ff[2],:])/3
            G[pp][dd][2,3] = p[dd]
            p = (2*mesh.v[ff[3],:] + mesh.v[ff[2],:])/3
            G[pp][dd][3,1] = p[dd]
            p = (mesh.v[ff[3],:] + 2*mesh.v[ff[2],:])/3
            G[pp][dd][3,2] = p[dd]

            p = (4*mesh.v[ff[0],:] + 2*mesh.v[ff[1],:] + mesh.v[ff[2],:] + 2*mesh.v[ff[3],:])/9
            G[pp][dd][1,1] = p[dd]
            p = (2*mesh.v[ff[0],:] + 4*mesh.v[ff[1],:] + 2*mesh.v[ff[2],:] + mesh.v[ff[3],:])/9
            G[pp][dd][1,2] = p[dd]
            p = (mesh.v[ff[0],:] + 2*mesh.v[ff[1],:] + 4*mesh.v[ff[2],:] + 2*mesh.v[ff[3],:])/9
            G[pp][dd][2,2] = p[dd]
            p = (2*mesh.v[ff[0],:] + mesh.v[ff[1],:] + 2*mesh.v[ff[2],:] + 4*mesh.v[ff[3],:])/9
            G[pp][dd][2,1] = p[dd]
    return G

def refine(G, mesh):
    [i,j,_] = find(mesh.e)
    [_,idx,_] = find(i<j)
    nE = len(idx)
    row = np.zeros(nE,dtype = int)
    col = np.zeros(nE,dtype = int)
    data = np.zeros(nE,dtype = int)
    for k in range(nE):
        row[k] = i[idx[k]]
        col[k] = j[idx[k]]
        data[k] = k
    edge_idx = coo_matrix((data,(row,col)), shape=(mesh.nv,mesh.nv)).tocsr()
    
    dim = len(mesh.v[0,:])
    nP = mesh.nf
    nV = mesh.nv
    Gref = [None]*(4*nP);
    v = np.zeros([nV+nP+nE,dim],dtype=float)
    f = np.zeros([4*nP,4],dtype=int)
    for pp in range(nP):
        DB = subdivideBezier(G[pp])
#         |-3-|-4-|
#         |-1-|-2-|
#         | u; -- v
        Gref[4*pp] = DB[0];
        Gref[4*pp+1] = DB[1];
        Gref[4*pp+2] = DB[2];
        Gref[4*pp+3] = DB[3];
        
        v1 = mesh.f[pp,0]
        v2 = mesh.f[pp,1]
        v3 = mesh.f[pp,2]
        v4 = mesh.f[pp,3]
        v5 = (edge_idx[v1,v2] if (v1 < v2) else edge_idx[v2,v1]) + mesh.nv
        v6 = (edge_idx[v2,v3] if (v2 < v3) else edge_idx[v3,v2]) + mesh.nv
        v7 = (edge_idx[v3,v4] if (v3 < v4) else edge_idx[v4,v3]) + mesh.nv
        v8 = (edge_idx[v4,v1] if (v4 < v1) else edge_idx[v1,v4]) + mesh.nv
        v9 = nV+nE+pp 
        f[4*pp] = np.array([v1,v5,v9,v8])
        f[4*pp+1] = np.array([v5,v2,v6,v9])
        f[4*pp+2] = np.array([v8,v9,v7,v4])
        f[4*pp+3] = np.array([v9,v6,v3,v7])
    
        for k in range(4):
            for i in range(dim-1):
                DB[k][i] = DB[k][i]*DB[k][dim-1]
        for k in range(dim):
            v[v5,k] = 4*DB[0][k][1,2]-2*DB[0][k][1,1]-2*DB[0][k][2,2]+DB[0][k][2,1];
            v[v6,k] = 4*DB[1][k][2,2]-2*DB[1][k][1,2]-2*DB[1][k][2,1]+DB[1][k][1,1];
            v[v7,k] = 4*DB[2][k][2,2]-2*DB[2][k][1,2]-2*DB[2][k][2,1]+DB[2][k][1,1];
            v[v8,k] = 4*DB[0][k][2,1]-2*DB[0][k][1,1]-2*DB[0][k][2,2]+DB[0][k][1,2];
            v[v9,k] = 4*DB[0][k][2,2]-2*DB[0][k][2,1]-2*DB[0][k][1,2]+DB[0][k][1,1];
            v[v1,k] = 4*DB[0][k][1,1]-2*DB[0][k][2,1]-2*DB[0][k][1,2]+DB[0][k][2,2];
            v[v2,k] = 4*DB[1][k][1,2]-2*DB[1][k][1,1]-2*DB[1][k][2,2]+DB[1][k][2,1];
            v[v3,k] = 4*DB[3][k][2,2]-2*DB[3][k][2,1]-2*DB[3][k][1,2]+DB[3][k][1,1];
            v[v4,k] = 4*DB[2][k][2,1]-2*DB[2][k][1,1]-2*DB[2][k][2,2]+DB[2][k][1,2];

            if (mesh.isboundaryv[v1] and mesh.isboundaryv[v2]):
                if (mesh.valence[v1] > 2):
                    v[v1,k] = 2*DB[0][k][1,1] - DB[0][k][1,2];
                else:
                    v[v1,k] = DB[0][k][1,1];
                v[v5,k] = 2*DB[1][k][1,1] - DB[1][k][1,2];
                if (mesh.valence[v2] > 2):
                    v[v2,k] = 2*DB[1][k][1,2] - DB[1][k][1,1];
                else:
                    v[v2,k] = DB[1][k][1,2];
            if (mesh.isboundaryv[v2] and mesh.isboundaryv[v3]):
                if (mesh.valence[v2] > 2):
                    v[v2,k] = 2*DB[1][k][1,2] - DB[1][k][2,2];
                else:
                    v[v2,k] = DB[1][k][1,2];
                v[v6,k] = 2*DB[3][k][1,2] - DB[3][k][2,2];
                if (mesh.valence[v3] > 2):
                    v[v3,k] = 2*DB[3][k][2,2] - DB[3][k][1,2];
                else:
                    v[v3,k] = DB[3][k][2,2];
            if (mesh.isboundaryv[v3] and mesh.isboundaryv[v4]):
                if (mesh.valence[v3] > 2):
                    v[v3,k] = 2*DB[3][k][2,2] - DB[3][k][2,1];
                else:
                    v[v3,k] = DB[3][k][2,2];
                v[v7,k] = 2*DB[2][k][2,2] - DB[2][k][2,1];
                if (mesh.valence[v4] > 2):
                    v[v4,k] = 2*DB[2][k][2,1] - DB[2][k][2,2]
                else:
                    v[v4,k] = DB[2][k][2,1];
            if (mesh.isboundaryv[v1] and mesh.isboundaryv[v4]):
                if (mesh.valence[v4]>2):
                    v[v4,k] = 2*DB[2][k][2,1] - DB[2][k][1,1];
                else:
                    v[v4,k] = DB[2][k][2,1];
                if (mesh.valence[v1]>2):
                    v[v1,k] = 2*DB[0][k][1,1] - DB[0][k][2,1];
                else:
                    v[v1,k] = DB[0][k][1,1];
                v[v8,k] = 2*DB[2][k][1,1] - DB[2][k][2,1];
    for d in range(dim):          
        v[:,d] = v[:,d]/v[:,dim-1]
    
    return Gref, Mesh(v,f)

def poissonSolver(G,DoFM,nDoF,nPat,bdy_ind,f):
    A = np.zeros([nDoF,nDoF])
    rhs = np.zeros(nDoF)
    nG = 5 # Gaussian quadrature stencils
    gauss_w = np.array([0.2369268850561891, 0.4786286704993665, 0.5688888888888889, 0.4786286704993665, 0.2369268850561891])
    gauss_p = np.array([-0.9061798459386640, -0.5384693101056831, 0.0, 0.5384693101056831, 0.9061798459386640])
    u = 0.5 + 0.5*gauss_p  # map to [0..1]
    n = 3 
    n1 = n + 1
    
    b = np.zeros([n1, nG])
    db = np.zeros([n, nG])
    for i in range(n1):
        b[i,:] = nCk(n,i)*(np.power(1-u,n-i)*np.power(u,i))
        if (i < n):
            db[i,:] = n*nCk(n-1,i)*(np.power(1-u,n-1-i)*np.power(u,i))
    # precompute Bezier basis functions, dont care about rational functions
    cz = np.zeros([n1,n1]);
    Z = [None]*n1;
    Zu = [None]*n1;
    Zv = [None]*n1;
    for i1 in range(n1):
        Z[i1] = [None]*n1
        Zu[i1] = [None]*n1
        Zv[i1] = [None]*n1
        for j1 in range(n1):
            cz[i1,j1] = 1.0;
            d1cz = cz[1:n1,:]-cz[0:n,:] 
            d2cz = cz[:,1:n1]-cz[:,0:n]
            Z[i1][j1] = np.dot(np.dot(np.transpose(b),cz ),b)
            Zu[i1][j1] = np.dot(np.dot(np.transpose(db),d1cz ),b)
            Zv[i1][j1] = np.dot(np.dot(np.transpose(b),d2cz ),db)
            cz[i1,j1] = 0.0
    dim = len(G[0])
    for pp in range(nPat):
        cx = G[pp][0]
        cy = G[pp][1]
        cw = G[pp][dim-1]
        X = np.dot(np.dot(np.transpose(b),(cx*cw) ),b)
        Y = np.dot(np.dot(np.transpose(b),(cy*cw) ),b)
        W = np.dot(np.dot(np.transpose(b),cw),b)
        X = X/W 
        Y = Y/W;
        d1cx = cx[1:n1,:]-cx[0:n,:] 
        d2cx = cx[:,1:n1]-cx[:,0:n]
        d1cy = cy[1:n1,:]-cy[0:n,:] 
        d2cy = cy[:,1:n1]-cy[:,0:n]
        d1cw = cw[1:n1,:]-cw[0:n,:] 
        d2cw = cw[:,1:n1]-cw[:,0:n]
        Wu = np.dot(np.dot(np.transpose(db),d1cw),b)
        Wv = np.dot(np.dot(np.transpose(b),d2cw),db)
    
        Xu = (np.dot(np.dot(np.transpose(db),d1cx),b) - X*Wu)/W # nGxnG 
        Xv = (np.dot(np.dot(np.transpose(b),d2cx),db) - X*Wv)/W # nGxnG
        Yu = (np.dot(np.dot(np.transpose(db),d1cy),b) - Y*Wu)/W # nGxnG
        Yv = (np.dot(np.dot(np.transpose(b),d2cy),db) - Y*Wv)/W # nGxnG
        dJ = Xu*Yv - Xv*Yu
        
        for i1 in range(n1):
            for i2 in range(n1):
                Zix = (Yv*Zu[i1][i2] - Yu*Zv[i1][i2])/dJ
                Ziy = (Xu*Zv[i1][i2] - Xv*Zu[i1][i2])/dJ
                dofi = DoFM[pp][i1][i2][0] # degrees of freedom
                sci = DoFM[pp][i1][i2][1] # weights of contribution
                for j1 in range(n1):
                    for j2 in range(n1):
                        Zjx = (Yv*Zu[j1][j2] - Yu*Zv[j1][j2])/dJ
                        Zjy = (Xu*Zv[j1][j2] - Xv*Zu[j1][j2])/dJ
                        dofj = DoFM[pp][j1][j2][0]
                        scj = DoFM[pp][j1][j2][1]
                        A[np.ix_(dofi,dofj)] = A[np.ix_(dofi,dofj)] + 0.25*(np.transpose([sci])*scj)*np.dot(np.dot(gauss_w,(Zix*Zjx + Ziy*Zjy)*np.absolute(dJ)),np.transpose([gauss_w]))
                rhs[dofi] = rhs[dofi] + 0.25*np.dot(np.dot(gauss_w, f(X,Y)*Z[i1][i2]*np.absolute(dJ)),np.transpose([gauss_w]))*sci; 
    #homogeneous boundary conditions
    A[:,bdy_ind] = 0;
    A[bdy_ind,:] = 0;
    A[bdy_ind,bdy_ind] = 1;
    rhs[bdy_ind] = 0;
    z = linalg.solve(A,rhs)
    return A,rhs,z

def postProcess(z,G,DoFM,nDoF,nPat,sol,solx,soly,shwErr = True):
    nG = 5 # Gaussian quadrature stencils
    gauss_w = np.array([0.2369268850561891, 0.4786286704993665, 0.5688888888888889, 0.4786286704993665, 0.2369268850561891])
    gauss_p = np.array([-0.9061798459386640, -0.5384693101056831, 0.0, 0.5384693101056831, 0.9061798459386640])
    u = 0.5 + 0.5*gauss_p  # map to [0..1]
    n = 3 
    n1 = n + 1
    b = np.zeros([n1, nG])
    db = np.zeros([n, nG])
    for i in range(n1):
        b[i,:] = nCk(n,i)*(np.power(1-u,n-i)*np.power(u,i))
        if (i < n):
            db[i,:] = n*nCk(n-1,i)*(np.power(1-u,n-1-i)*np.power(u,i))
    dim = len(G[0])
    L2 = 0
    L8 = 0
    H1 = 0
    fig = figure('Analysis ')
    fig.scene.disable_render = True
    for pp in range(nPat):
        cx = G[pp][0]
        cy = G[pp][1]
        cw = G[pp][dim-1]
        X = np.dot(np.dot(np.transpose(b),(cx*cw) ),b)
        Y = np.dot(np.dot(np.transpose(b),(cy*cw) ),b)
        W = np.dot(np.dot(np.transpose(b),cw),b)
        X = X/W 
        Y = Y/W;
        d1cx = cx[1:n1,:]-cx[0:n,:] 
        d2cx = cx[:,1:n1]-cx[:,0:n]
        d1cy = cy[1:n1,:]-cy[0:n,:] 
        d2cy = cy[:,1:n1]-cy[:,0:n]
        d1cw = cw[1:n1,:]-cw[0:n,:] 
        d2cw = cw[:,1:n1]-cw[:,0:n]
        Wu = np.dot(np.dot(np.transpose(db),d1cw),b)
        Wv = np.dot(np.dot(np.transpose(b),d2cw),db)
    
        Xu = (np.dot(np.dot(np.transpose(db),d1cx),b) - X*Wu)/W # nGxnG 
        Xv = (np.dot(np.dot(np.transpose(b),d2cx),db) - X*Wv)/W # nGxnG
        Yu = (np.dot(np.dot(np.transpose(db),d1cy),b) - Y*Wu)/W # nGxnG
        Yv = (np.dot(np.dot(np.transpose(b),d2cy),db) - Y*Wv)/W # nGxnG
        dJ = Xu*Yv - Xv*Yu
        cz = np.zeros_like(cx)
        for i1 in range(n1):
            for i2 in range(n1):
                dof = DoFM[pp][i1][i2][0]
                w = DoFM[pp][i1][i2][1]
                cz[i1,i2] = np.dot(w,z[dof])
        Z = np.dot(np.dot(np.transpose(b),cz ),b)
        d1cz = cz[1:n1,:]-cz[0:n,:] 
        d2cz = cz[:,1:n1]-cz[:,0:n]
        Zu = np.dot(np.dot(np.transpose(db),d1cz ),b)
        Zv = np.dot(np.dot(np.transpose(b),d2cz ),db)
        Zx = (Yv*Zu - Yu*Zv)/dJ
        Zy = (Xu*Zv - Xv*Zu)/dJ
        err = sol(X,Y) - Z
        err_x = solx(X,Y) - Zx
        err_y = soly(X,Y) - Zy
        L2 = L2 + 0.25*np.dot(np.dot(gauss_w, (err**2)*np.absolute(dJ)),np.transpose([gauss_w]))
        H1 = H1 + 0.25*np.dot(np.dot(gauss_w, (err_x**2 + err_y**2)*np.absolute(dJ)),np.transpose([gauss_w])) 
        L8 = max(L8,np.amax(np.absolute(err)))
        if shwErr:
            mesh(X,Y,err)
        else:
            mesh(X,Y,Z)
    fig.scene.disable_render = False
    H1 = H1 + L2
    L2 = np.sqrt(L2)
    H1 = np.sqrt(H1)
    return L8,L2,H1    
    