#!/usr/bin/python
from iga import *
import warnings
warnings.filterwarnings("ignore")

[v,f] = read_off('sqr_3.off')
mesh = Mesh(v,f)
mesh.v[:,1] = mesh.v[:,1]*6
mesh.v[:,0] = mesh.v[:,0]*6
# mesh = mesh.refine()
G = genBilinear(mesh)
# D = DeCasteljau(G[0],'u',0.5)
# patchView(D[0],11,'wireframe',(0,1,0))
# patchView(D[1],11,'wireframe',(0,0,1))
# [D1,D2,D3,D4] = subdivideBezier(G[0])
# patchView(D1,11,'wireframe',(0,0,1))
# patchView(D2,11,'wireframe',(0,1,1))
# patchView(D3,11,'wireframe',(0,1,0))
# patchView(D4,11,'wireframe',(1,1,0))
[G,mesh] = refine(G,mesh)

# mesh.draw()
[G, DoFM, nDoF, nPat, bdy_ind, bdy_map] = bezierExtraction(G,mesh)

# bezierView(G,5,'wireframe')
# for i in range(nPat):
#     patchView(G[i],5,'wireframe')

fig = basisView(G,nDoF,DoFM,1,11)

# def f(x,y):
#     return (4*np.pi*np.pi/9)*np.sin((np.pi*x)/3)*np.sin((np.pi*y)/3)
# 
# [A,rhs,z] = poissonSolver(G,DoFM,nDoF,nPat,bdy_ind,f)
# 
# # print A[:25,:25]
# # print z[:25]
# 
# def sol(x,y):
#     return 2*np.sin((np.pi*x)/3)*np.sin((np.pi*y)/3)
# def solx(x,y):
#     return (2*np.pi/3)*np.cos((np.pi*x)/3)*np.sin((np.pi*y)/3)
# def soly(x,y):
#     return (2*np.pi/3)*np.sin((np.pi*x)/3)*np.cos((np.pi*y)/3)
#  
# [L8,L2,H1] = postProcess(z,G,DoFM,nDoF,nPat,sol,solx,soly)
# print L8,L2,H1

raw_input("Press Enter to continue...")


